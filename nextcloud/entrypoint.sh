#!/bin/bash
NEXTCLOUD_UPDATE=1 /entrypoint.sh true

php occ upgrade
php occ db:add-missing-indices
php occ db:add-missing-primary-keys
php occ app:update --all

exec php-fpm
